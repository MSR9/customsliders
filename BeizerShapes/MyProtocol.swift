//
//  MyProtocol.swift
//  BeizerShapes
//
//  Created by PrahladReddy on 2/25/20.
//  Copyright © 2020 PrahladReddy. All rights reserved.
//

import Foundation
import UIKit

protocol demoviewDelegate : class {
    func sliderMoved(_ sender : DemoView, percentage : CGFloat)
}
