//
//  DemoView.swift
//  BeizerShapes
//
//  Created by PrahladReddy on 2/25/20.
//  Copyright © 2020 PrahladReddy. All rights reserved.
//

import UIKit

enum SliderColor {
    case redSlider
    case blueSlider
    case greenSlider
    case alphaSlider
}

class DemoView: UIView {
    
    let vc = ViewController()
    var colorFlag: SliderColor = .greenSlider
    weak var delegate : demoviewDelegate?
    
    var percentage : CGFloat = 0 {
        didSet {
            self.delegate?.sliderMoved(self, percentage: percentage)
        }
    }
    var path : UIBezierPath!
    var cursor = UIView()
    
    func addCursorView() {
        addSubview(cursor)
        self.cursor.frame.size = .init(width: 8, height: self.frame.size.height)
        self.cursor.layer.borderWidth = 2
        self.cursor.layer.cornerRadius = self.cursor.frame.size.width / 2
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    init(frame: CGRect, color: SliderColor) {
        super.init(frame: frame)
        colorFlag = color
        self.backgroundColor = UIColor.white
        self.addCursorView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func createTriangle(color: UIColor) {
        
        path = UIBezierPath()
        
        path.move(to: CGPoint(x: 0.0, y: frame.size.height))
        
        path.addLine(to: CGPoint(x:self.frame.size.width, y: 0.0))
        
        path.addLine(to: CGPoint(x: self.frame.size.width, y: self.frame.size.height))
        
        path.close()
        
        color.setFill()
        path.fill()
        
        UIColor.black.setStroke()
        path.stroke()
    }
    
    
    override func draw(_ rect: CGRect) {
        switch colorFlag {
        case .blueSlider:
            createTriangle(color: .blue)
        case .greenSlider:
            createTriangle(color: .green)
        case .redSlider:
            createTriangle(color: .red)
        case .alphaSlider:
            createTriangle(color: .gray)
        }
    }
}

extension DemoView {
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        self.cursor.layer.borderColor = UIColor.green.cgColor
        
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesMoved(touches, with: event)
        guard let touch = touches.first else { return }
        let touchPoint = touch.location(in: self)
        
        var x = touchPoint.x
        
        if x > frame.width {
            x = frame.width
        } else if  x < 0 {
            x = 0
        }
        
        self.cursor.center.x = x
        
        self.percentage = x / frame.width
        
        self.cursor.setNeedsDisplay()
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        self.cursor.layer.borderColor = UIColor.black.cgColor
    }
}
