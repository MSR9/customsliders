//
//  ViewController.swift
//  BeizerShapes
//
//  Created by PrahladReddy on 2/25/20.
//  Copyright © 2020 PrahladReddy. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var labelValue: String?
    var demoViewlabel1: UILabel?
    var demoViewlabel2: UILabel?
    var demoViewlabel3: UILabel?
    var demoViewlabel4: UILabel?
    
    var color: UIColor?
    
    var labelColorR: UILabel?
    var labelColorG: UILabel?
    var labelColorB: UILabel?
    var labelColorA: UILabel?
    
    var blueSlider: DemoView?
    var redSlider: DemoView?
    var greenSlider: DemoView?
    var alphaSlider: DemoView?
    
    var colorpreview = UIView()
    var bluevalue : CGFloat = 0
    var redvalue : CGFloat = 0
    var greenvalue : CGFloat = 0
    var alpha : CGFloat = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        labelColorR = UILabel(frame: CGRect(x: 0.0, y: 0.0, width: 45, height: 40))
        labelColorR?.center = CGPoint(x: 50, y: 730)
        labelColorR?.textAlignment = .center
        labelColorR?.text = "R"
        labelColorR?.layer.backgroundColor = UIColor.lightGray.cgColor
        
        labelColorG = UILabel(frame: CGRect(x: 0.0, y: 0.0, width: 45, height: 40))
        labelColorG?.center = CGPoint(x: 50, y: 590)
        labelColorG?.textAlignment = .center
        labelColorG?.text = "A"
        labelColorG?.layer.backgroundColor = UIColor.lightGray.cgColor
        
        labelColorB = UILabel(frame: CGRect(x: 0.0, y: 0.0, width: 45, height: 40))
        labelColorB?.center = CGPoint(x: 50, y: 450)
        labelColorB?.textAlignment = .center
        labelColorB?.text = "B"
        labelColorB?.layer.backgroundColor = UIColor.lightGray.cgColor
        
        labelColorA = UILabel(frame: CGRect(x: 0.0, y: 0.0, width: 45, height: 40))
        labelColorA?.center = CGPoint(x: 50, y: 310)
        labelColorA?.textAlignment = .center
        labelColorA?.text = "G"
        labelColorA?.layer.backgroundColor = UIColor.lightGray.cgColor
        
        
        demoViewlabel1 = UILabel(frame: CGRect(x: 0.0, y: 0.0, width: 45, height: 40))
        demoViewlabel1?.center = CGPoint(x: 360, y: 590)
        demoViewlabel1?.textAlignment = .center
        demoViewlabel1?.text = labelValue ?? "0"
        demoViewlabel1?.layer.backgroundColor = UIColor.lightGray.cgColor
        
        demoViewlabel2 = UILabel(frame: CGRect(x: 0.0, y: 0.0, width: 45, height: 40))
        demoViewlabel2?.center = CGPoint(x: 360, y: 450)
        demoViewlabel2?.textAlignment = .center
        demoViewlabel2?.text = labelValue ?? "0"
        demoViewlabel2?.layer.backgroundColor = UIColor.lightGray.cgColor
        
        demoViewlabel3 = UILabel(frame: CGRect(x: 0.0, y: 0.0, width: 45, height: 40))
        demoViewlabel3?.center = CGPoint(x: 360, y: 490)
        demoViewlabel3?.textAlignment = .center
        demoViewlabel3?.text = labelValue ?? "0"
        demoViewlabel3?.layer.backgroundColor = UIColor.lightGray.cgColor
        
        demoViewlabel3 = UILabel(frame: CGRect(x: 0.0, y: 0.0, width: 45, height: 40))
        demoViewlabel3?.center = CGPoint(x: 360, y: 310)
        demoViewlabel3?.textAlignment = .center
        demoViewlabel3?.text = labelValue ?? "0"
        demoViewlabel3?.layer.backgroundColor = UIColor.lightGray.cgColor
        
        demoViewlabel4 = UILabel(frame: CGRect(x: 0.0, y: 0.0, width: 45, height: 40))
        demoViewlabel4?.center = CGPoint(x: 360, y: 730)
        demoViewlabel4?.textAlignment = .center
        demoViewlabel4?.text = labelValue ?? "0"
        demoViewlabel4?.layer.backgroundColor = UIColor.lightGray.cgColor
        
        self.view.addSubview(demoViewlabel1!)
        self.view.addSubview(demoViewlabel2!)
        self.view.addSubview(demoViewlabel3!)
        self.view.addSubview(demoViewlabel4!)
        
        self.view.addSubview(labelColorR!)
        self.view.addSubview(labelColorG!)
        self.view.addSubview(labelColorB!)
        self.view.addSubview(labelColorA!)
    }
    
    func updatePreviewBox() {
        colorpreview.backgroundColor = UIColor(red: redvalue, green: greenvalue, blue: bluevalue, alpha: alpha)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let width: CGFloat = 240.0
        let height: CGFloat = 60
        
        colorpreview.frame = CGRect(origin: .init(x: 60, y: 100), size: CGSize(width: 300/*view.frame.size.width*/, height: 100))
        
        alphaSlider = DemoView(frame: CGRect(x: self.view.frame.size.width/2 - width/2,
                                             y: self.view.frame.size.height/1.5 - height/1.5,
                                             width: width,
                                             height: height), color: .alphaSlider)
        
        
        blueSlider = DemoView(frame: CGRect(x: self.view.frame.size.width/2 - width/2,
                                            y: self.view.frame.size.height/2 - height/2,
                                            width: width,
                                            height: height), color: .blueSlider)
        
        
        greenSlider = DemoView(frame: CGRect(x: self.view.frame.size.width/2 - width/2,
                                             y: self.view.frame.size.height/3 - height/3,
                                             width: width,
                                             height: height), color: .greenSlider)
        
        
        redSlider = DemoView(frame: CGRect(x: self.view.frame.size.width/2 - width/2,
                                           y: self.view.frame.size.height/1.2 - height/1.2,
                                           width: width,
                                           height: height), color: .redSlider)
        
        
        alphaSlider?.delegate = self
        blueSlider?.delegate = self
        greenSlider?.delegate = self
        redSlider?.delegate = self
        
        self.view.addSubview(alphaSlider!)
        self.view.addSubview(blueSlider!)
        self.view.addSubview(greenSlider!)
        self.view.addSubview(redSlider!)
        self.view.addSubview(colorpreview)
        updatePreviewBox()
    }
}

extension ViewController: demoviewDelegate {
    func sliderMoved(_ sender: DemoView, percentage: CGFloat) {
        switch sender {
        case alphaSlider:
            demoViewlabel1?.text = percentage.description
            alpha = percentage
        case blueSlider:
            demoViewlabel2?.text = String(format: "%d", Int(percentage * 255))
            self.bluevalue = percentage
        case greenSlider:
            demoViewlabel3?.text = String(format: "%d", Int(percentage * 255))
            self.greenvalue = percentage
        case redSlider:
            demoViewlabel4?.text = String(format: "%d", Int(percentage * 255))
            self.redvalue = percentage
        default: break
        }
        updatePreviewBox()
    }
}
